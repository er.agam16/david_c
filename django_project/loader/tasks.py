from celery import shared_task
from .models import Loader
import boto3
import csv
from django_project.constants import BUCKET_NAME, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY
from redshift.api import *
from .models import LoaderStatus


@shared_task
def shift_files(user_id, s3_urls, time_stamp, redshift_id, table_name, extra_details):
    for s3_url in s3_urls:
        Loader.objects.create(
            user_id=user_id, bundle_id=time_stamp, s3_url=s3_url)
    data = validate_csv(s3_urls, extra_details)
    if data[0]:
        print('validated')
        process_all_csv(user_id, s3_urls, redshift_id, table_name,
                        data[1].get('headers', []), time_stamp)
    else:
        pass


def process_all_csv(user_id, s3_urls, redshift_id, table_name, headers, time_stamp):
    for s3_url in s3_urls:
        result = process_csv(user_id, redshift_id,
                             table_name, s3_url, headers, time_stamp)
        print(result)


def process_csv(user_id, redshift_id, table_name, s3_url, headers, time_stamp):
    loaders = Loader.objects.filter(
        user_id=user_id, bundle_id=time_stamp, s3_url=s3_url)
    if loaders:
        loader = loaders.first()
    conn, message = get_redshidt_connection(redshift_id)
    if not conn:
        loaders = Loader.objects.filter(
        user_id=user_id, bundle_id=time_stamp, s3_url=s3_url).update(status=2,status_detail="Failed - DB Connection Error")
        return False, message
    if (not check_table_exist(redshift_id, table_name)):
        create_query = get_create_query(headers, table_name)
        cur = conn.cursor()
        try:
            cur.execute(create_query)
            cur.close()
            conn.commit()
        except:
            message = 'Error in Creating Table.'
            print(traceback.format_exc())
            print(sys.exc_info())
            conn.rollback()
            msg = traceback.format_exc()
            loader.status = 2
            loader.status_detail = msg
            loader.save()
            return False, msg
    status, error = insert_into_redshift(
        redshift_id, table_name, s3_url)
    if status:
        # update_meta data
        loader.status = 1
        loader.status_detail = 'Success'
        loader.save()
    else:
        loader.status = 2
        loader.status_detail = 'Error - ' + error
        loader.save()
    return status, error


def validate_csv(s3_urls, extra_details):
    s3 = boto3.resource('s3', aws_access_key_id=AWS_ACCESS_KEY_ID,
                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
    is_header = extra_details.get('is_header', True)
    delimeter = extra_details.get('delimeter', ',')
    csv_line_spliter = extra_details.get('csv_line_spliter', '\n')
    all_headers = []
    all_num_cols = []
    for s3_url in s3_urls:
        print(s3_url)
        s3obj = s3.Object(BUCKET_NAME, s3_url)
        filedata = s3obj.get()["Body"].read()
        if is_header:
            all_headers.append(get_headers(
                filedata, delimeter, csv_line_spliter))
        all_num_cols.append(get_number_of_col(
            filedata, delimeter, csv_line_spliter))
    is_valid_header = all(x == all_headers[0] for x in all_headers)
    is_valid_number_col = all(x == all_num_cols[0] for x in all_num_cols)
    return (is_valid_header and is_valid_number_col, {'headers': all_headers[0]})


def get_headers(filedata, delimeter, csv_line_spliter):
    try:
        first_line = filedata.decode('utf-8').split(csv_line_spliter)[0]
        headers = first_line.split(delimeter)
        print(first_line)
        print(headers)
    except:
        headers = []
    return headers


def get_number_of_col(filedata, delimeter, csv_line_spliter):
    try:
        first_line = filedata.decode('utf-8').split(csv_line_spliter)[0]
        return len(first_line.split(delimeter))
    except:
        return 0
