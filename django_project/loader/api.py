from rest_framework import generics, status
from django_project import key_config
from rest_framework.response import Response
# from rest_framework.parsers import FileUploadParser
from django.contrib.auth import get_user_model
from rest_framework import permissions
import tinys3
User = get_user_model()
from datetime import datetime
from .tasks import shift_files, validate_csv
from .serializers import LoaderSerializer, LoaderListSerializer
from .models import Loader
import requests
try:
    import urllib.parse as urllib
except:
    import urllib
import boto3
from django_project.constants import BUCKET_NAME, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, SCHEMA


class MultipleFileToRedShift(generics.GenericAPIView):
    # parser_classes = (FileUploadParser,)
    serializer_class = LoaderSerializer

    def post(self, request):
        s3_urls = request.POST.get('data', '').split(',')
        is_header = request.POST.get("is_header", False)
        delimiter = request.POST.get("delimiter", ",")
        redshift_id = self.request.data.get('redshift_id', 3)
        table_name = self.request.data.get('table_name', 'table_name')
        table_name = SCHEMA + '.' + table_name
        timestamp = str(datetime.now().strftime('%Y_%m_%d_%H_%M_%S'))
        extra_details = {}
        extra_details['is_header'] = is_header
        extra_details['delimiter'] = delimiter
        # import pdb
        # pdb.set_trace()
        s3_urls = [urllib.unquote(i) for i in s3_urls]
        shift_files.delay(request.user.id, s3_urls, timestamp,
                          redshift_id, table_name, extra_details)
        return Response({'deatils': 'In progress'},
                        status=status.HTTP_200_OK)


class LoaderList(generics.ListCreateAPIView):
    queryset = Loader.objects.all()
    serializer_class = LoaderListSerializer

    def list(self, request):

        queryset = self.get_queryset()
        if not request.user.is_anonymous():
            queryset = queryset.filter(user=request.user)
        serializer = LoaderListSerializer(queryset, many=True)
        return Response(serializer.data)


class MultipleFileToRedShiftValidate(generics.ListCreateAPIView):
    queryset = Loader.objects.all()
    serializer_class = LoaderSerializer

    def post(self, request):
        s3_urls = request.POST.get('data', '').split(',')
        is_header = request.POST.get("is_header", False)
        delimiter = request.POST.get("delimiter", ",")
        redshift_id = self.request.data.get('redshift_id', 3)
        table_name = self.request.data.get('table_name', 'table_name')
        table_name = SCHEMA + '.' + table_name
        extra_details = {}
        extra_details['is_header'] = is_header
        extra_details['delimiter'] = delimiter
        csv_line_spliter = extra_details.get('csv_line_spliter', '\n')
        s3_urls = [urllib.unquote(i) for i in s3_urls]
        is_validated, dic_extra_info = validate_csv(s3_urls, extra_details)
        if is_validated and s3_urls and dic_extra_info.get('headers'):
            headers = dic_extra_info.get('headers')
            s3_url = s3_urls[0]
            s3 = boto3.resource('s3', aws_access_key_id=AWS_ACCESS_KEY_ID,
                                aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
            s3obj = s3.Object(BUCKET_NAME, s3_url)
            filedata = s3obj.get()["Body"].read()
            csv_lines = filedata.decode('utf-8').split(csv_line_spliter)
            cols = []
            import csv
            for index, lines in enumerate(csv_lines):
                if index > 0:
                    for line in csv.reader([lines], delimiter=',', quotechar='"', skipinitialspace=True):
                        cols.append(line)
                if index > 50:
                    break
            return Response({'csv_data': cols, 'csv_header': headers})
        else:
            return Response({'details': 'Not validated'}, status=status.HTTP_400_BAD_REQUEST)