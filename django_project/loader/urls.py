from django.conf.urls import url
from . import api


urlpatterns = [
    url(r'^(?i)api/MultipleFileToRedShift/', api.MultipleFileToRedShift.as_view()),

    url(r'^(?i)api/LoaderList/', api.LoaderList.as_view()),

    url(r'^(?i)api/MultipleFileToRedShiftValidate/', api.MultipleFileToRedShiftValidate.as_view()),
    
]