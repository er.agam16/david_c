# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
# # -*- coding: utf-8 -*-
# from __future__ import unicode_literals
import uuid
from django.utils import timezone
from datetime import timedelta

from django.db import models

# Create your models here.
from django.conf import settings
from django_project.models import TimestampModel
from django.contrib.auth import get_user_model
from django_enumfield import enum

User = get_user_model()

class LoaderStatus(enum.Enum):
    IN_PROGRESS = 0
    DONE = 1
    FAILED = 2

# Create your models here.
class Loader(TimestampModel):
    user = models.ForeignKey(User)
    bundle_id = models.CharField(max_length=200)
    s3_url = models.CharField(max_length=400,null=True,blank=True)
    status = enum.EnumField(LoaderStatus, default=LoaderStatus.IN_PROGRESS)
    status_detail = models.TextField(max_length=200,null = True,blank = True, default="In Progress")

