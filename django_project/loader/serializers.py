from rest_framework import serializers
from redshift.models import RedShiftDb
from django.conf import settings
from django.contrib.auth import get_user_model
from .models import Loader
User = get_user_model()


class LoaderSerializer(serializers.Serializer):
    # s3_url = serializers.CharField()
    redshift_id = serializers.IntegerField()
    delimeter = serializers.CharField()
    csv_line_spliter = serializers.CharField()
    table_name = serializers.CharField()


class LoaderListSerializer(serializers.ModelSerializer):

    '''
    Serializer for LoaderListSerializer.
    '''
    class Meta:
        '''
        Serializer customization
        '''
        model = Loader
        fields = '__all__'