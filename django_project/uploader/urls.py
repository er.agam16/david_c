from django.conf.urls import url
from . import api


urlpatterns = [
    # url(r'upload$', api.FileUploadView.as_view())
    url(r'^upload/(?P<filename>[^/]+)$', api.FileUploadView.as_view()),
    url(r'^list', api.UserFilesList.as_view()),
]
