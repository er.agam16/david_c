from rest_framework import generics, status
from django_project import key_config
from rest_framework.response import Response
# from rest_framework.parsers import FileUploadParser
from django.contrib.auth import get_user_model
from rest_framework import permissions
import tinys3
User = get_user_model()
from django_project.constants import BUCKET_NAME, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY
from boto3.session import Session
import boto3

class FileUploadView(generics.GenericAPIView):
    # parser_classes = (FileUploadParser,)

    def post(self, request, filename='deafult_file', format=None):
        from datetime import datetime
        timestamp = str(datetime.now().strftime('%Y_%m_%d_%H_%M_%S'))
        file_obj = request.FILES['file']
        filename = file_obj._get_name()
        if filename.split('.csv'):
            filename = filename.split('.csv')[0] + '__' + timestamp + '.csv'
        self.upload_file_to_s3(
            file_obj, request.user.username + '/' + filename)
        return Response({'deatils': 'Uploaded Successfully'},
                        status=status.HTTP_200_OK)

    def upload_file_to_s3(self, file_obj, filename):
        S3_ACCESS_KEY = AWS_ACCESS_KEY_ID
        S3_SECRET_KEY = AWS_SECRET_ACCESS_KEY
        # Creating a simple connection
        conn = tinys3.Connection(S3_ACCESS_KEY, S3_SECRET_KEY)
        # Uploading a single file
        conn.upload(filename, file_obj, BUCKET_NAME)
        # Adding metadata for a key
        conn.update_metadata(
            filename, {'x-amz-meta-redshift-status': 'False'}, BUCKET_NAME)

    permission_classes = (permissions.IsAuthenticated,)


class UserFilesList(generics.ListAPIView):
    def get(self, request, *args, **kw):
        S3_ACCESS_KEY = AWS_ACCESS_KEY_ID
        S3_SECRET_KEY = AWS_SECRET_ACCESS_KEY

        session = Session(aws_access_key_id=S3_ACCESS_KEY,
                          aws_secret_access_key=S3_SECRET_KEY)
        s3 = session.resource('s3')
        your_bucket = s3.Bucket(BUCKET_NAME)
        botoConn = boto3.client(
            service_name='s3', aws_access_key_id=S3_ACCESS_KEY, aws_secret_access_key=S3_SECRET_KEY)
        list_files = []
        for s3_file in your_bucket.objects.all():
            if s3_file.key.startswith(request.user.username + '/') and s3_file.key.endswith('.csv'):
                metaInfo = botoConn.head_object(
                    Bucket=BUCKET_NAME, Key=s3_file.key)
                if 'redshift-status' in metaInfo['Metadata']:
                    uploaded = metaInfo['Metadata'][
                        'redshift-status']
                else:
                    uploaded = 'False'
                file = {'filename': s3_file.key,'uploaded' : uploaded, 'created': s3_file.last_modified}
                list_files.append(file)
        list_files.sort(key=lambda x: x['created'], reverse=True)
        return Response({'files': list_files},
                        status=status.HTTP_200_OK)
    permission_classes = (permissions.IsAuthenticated,)
