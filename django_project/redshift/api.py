import psycopg2
import tinys3
import boto3
import traceback
import sys
from django.conf import settings
from django_project.constants import BUCKET_NAME, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY,SCHEMA
# from .serializers import UserSerializer
from rest_framework import generics, status, permissions
from django.contrib.auth import authenticate
from django_project import key_config

from rest_framework.response import Response
from django.contrib.auth import get_user_model
from django.utils import timezone
from redshift.serializers import RedshiftDbSerializer, RedshiftDbCopySerializer , UserEmailSerializer
from .models import RedShiftDb,UserEmail
User = get_user_model()


def get_create_query(headers, table_name):
    base_query = 'CREATE TABLE {0} ({1})'
    cols = ''
    for header in headers:
        header = header.replace(u'\ufeff', '')
        header_new = ''.join(e for e in header if e.isalnum())
        cols = cols + ' "' + header_new + '" TEXT,'
    cols = cols[:-1]
    queryFo = base_query.format(table_name, cols)
    print(queryFo)
    return queryFo


def get_redshidt_connection(redshift_id):
    redshift = RedShiftDb.objects.filter(id=redshift_id).first()
    con = None
    try:
        if redshift:
            con = psycopg2.connect(dbname=redshift.name, host=redshift.host,
                                   port=redshift.port, user=redshift.username, password=redshift.password)
            message = 'Connected to the Selected Database'
    except:
        print(traceback.format_exc())
        print(sys.exc_info())
        con = None
        message = 'Unable to Connect to the Selected Database'
    return con, message


def check_table_exist(redshift_id, table_name):
    import re
    table_name = table_name.replace(SCHEMA + '.', '')
    check_query = "SELECT EXISTS( SELECT * FROM information_schema.tables WHERE table_schema = '{1}' AND (table_name = '{0}' OR table_name = LOWER('{0}')) )".format(
        table_name, SCHEMA)
    conn, message = get_redshidt_connection(redshift_id)
    if not conn:
        return False
    cur = conn.cursor()
    try:
        print(check_query)
        cur.execute(check_query)
        result = cur.fetchall()
        cur.close()
        conn.commit()
        print(result[0][0])
        return result[0][0]
    except:
        print(traceback.format_exc())
        print(sys.exc_info())
        # import pdb
        # pdb.set_trace()
        conn.rollback()
    return False


def insert_into_redshift(redshift_id, table_name, s3_url):
    conn, message = get_redshidt_connection(redshift_id)
    if not conn:
        return True, message
    cur = conn.cursor()
    errorQuery = """select le.starttime, d.query, d.line_number, d.colname, d.value,
le.raw_line, le.err_reason    
from stl_loaderror_detail d, stl_load_errors le
where d.query = le.query
order by le.starttime desc
limit 1"""
    main_s3_key = s3_url
    s3_url = 's3://{1}/{0}'.format(s3_url, BUCKET_NAME)
    print(s3_url)
    try:
        redshift = RedShiftDb.objects.filter(id=redshift_id).first()
        region = 'us-west-2'#redshift.host.split('.redshift')[0].split('.')[-1]
    except:
        region = 'us-west-2'
    copy_sql = """COPY {3} FROM '{0}'  CREDENTIALS 'aws_access_key_id={1};aws_secret_access_key={2}' delimiter ',' removequotes escape FILLRECORD IGNOREBLANKLINES region '{4}'""".format(
        s3_url, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, table_name, region)
    try:
        print(copy_sql)
        cur.execute(copy_sql)
        cur.close()
        conn.commit()
        s3_conn = tinys3.Connection(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
        # Adding metadata for a key
        s3_conn.update_metadata(
            main_s3_key, {'x-amz-meta-redshift-status': 'True'}, BUCKET_NAME)
        return True, 'Saved'
    except:
        conn.rollback()
        cur.execute(errorQuery)
        result = cur.fetchall()
        print(result)
        conn.commit()
        print(traceback.format_exc())
        print(sys.exc_info())
        conn.rollback()
        return False, 'Error in Shifting in Selected Database.'


class RedShiftDbList(generics.ListCreateAPIView):
    queryset = RedShiftDb.objects.all()
    serializer_class = RedshiftDbSerializer

    def list(self, request):

        queryset = self.get_queryset()
        if not request.user.is_anonymous():
            queryset = queryset.filter(user=request.user)
        serializer = RedshiftDbSerializer(queryset, many=True)
        return Response(serializer.data)


class RedShiftDbUpdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = RedShiftDb.objects.all()
    serializer_class = RedshiftDbSerializer


class CopyDataToRedShift(generics.GenericAPIView):
    serializer_class = RedshiftDbCopySerializer

    def post(self, request, *args, **kw):
        s3_url = self.request.data.get('s3_url', 1)
        file_type = self.request.data.get('file_type', 1)
        redshift_id = self.request.data.get('redshift_id', 3)
        delimeter = self.request.data.get('delimeter', ',')
        csv_line_spliter = self.request.data.get('csv_line_spliter', '\n')
        table_name = self.request.data.get('table_name', 'table_name')
        table_name = SCHEMA + '.' + table_name
        conn, message = get_redshidt_connection(redshift_id)
        if conn and s3_url:
            s3 = boto3.resource('s3', aws_access_key_id=AWS_ACCESS_KEY_ID,
                                aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
            s3obj = s3.Object(BUCKET_NAME, s3_url)
            filedata = s3obj.get()["Body"].read()
            try:
                first_line = filedata.decode(
                    'utf-8').split(csv_line_spliter)[0]
                headers = first_line.split(delimeter)
            except:
                return Response({'status': False, 'error': 'File Parsing Error'}, status=400)
            if (not check_table_exist(redshift_id, table_name)):
                create_query = get_create_query(headers, table_name)
                cur = conn.cursor()
                try:
                    cur.execute(create_query)
                    cur.close()
                    conn.commit()
                except:
                    message = 'Error in Creating Table.'
                    print(traceback.format_exc())
                    print(sys.exc_info())
                    # import pdb
                    # pdb.set_trace()
                    conn.rollback()
                    return Response({'status': False, 'error': message}, status=400)
            status, error = insert_into_redshift(
                redshift_id, table_name, s3_url)
            if status:
                return Response({'Data': 'Inserted'})
            else:
                return Response({'status': False, 'error': error}, status=400)
        else:
            return Response({'status': False, 'error': message}, status=400)


class UserEmailList(generics.ListCreateAPIView):
    queryset = UserEmail.objects.all()
    serializer_class = UserEmailSerializer


class RedShiftDbTest(generics.CreateAPIView):
    queryset = RedShiftDb.objects.all()
    serializer_class = RedshiftDbSerializer

    def post(self, request):
        redshift_id = self.request.data.get('redshift_id', 1)
        con, msg = get_redshidt_connection(redshift_id)
        if con:
            con = True
        else:
            con = False
        return Response({'status': con, 'msg': msg})
