from django.conf import settings

from django_project.constants import FRONTEND_URL, FROM_EMAIL, PRODUCT_NAME
from .serializers import UserSerializer, UserLogoutSerializer, ResetPasswordSerializer
from rest_framework import generics, status
from django.contrib.auth import authenticate
from django_project import key_config
from .models import AuthToken
from .serializers import UserLoginSerializer
from rest_framework.response import Response
from django.contrib.auth import get_user_model
from django.utils import timezone

from django.core.mail import send_mail
from django.template.loader import render_to_string
User = get_user_model()


class UserLogin(generics.GenericAPIView):
    serializer_class = UserSerializer

    def post(self, request, *args, **kw):
        username = self.request.data.get(key_config.KEY_USERNAME)
        password = self.request.data.get(key_config.KEY_PASSWORD)

        user = authenticate(username=username, password=password)
        response = {}
        if user:
            auth_token = AuthToken.objects.create(user=user)
            if auth_token:
                response = UserLoginSerializer(auth_token).data
                return Response(response, status=status.HTTP_200_OK)
        else:
            response['error'] = ['Invalid Credentials']
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)


class UserSignUp(generics.GenericAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def post(self, request, *args, **kw):
        response = {}
        username = self.request.data.get(key_config.KEY_USERNAME)
        password = self.request.data.get(key_config.KEY_PASSWORD)
        first_name = self.request.data.get(key_config.KEY_FIRST_NAME)
        last_name = self.request.data.get(key_config.KEY_LAST_NAME)

        user = User.objects.filter(
            username__iexact=username)
        if user:
            response['error'] = ['User-name Already Exist']
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        else:
            now = timezone.now()
            extra_data = {'first_name': first_name, 'last_name': last_name}
            user = User(username=username, email=username,
                        is_staff=False, is_active=True,
                        is_superuser=False,
                        date_joined=now,
                        **extra_data)
            user.set_password(password)
            user.save()
            if user:
                auth_token = AuthToken.objects.create(user=user)
                if auth_token:
                    response = UserLoginSerializer(auth_token).data
                    return Response(response, status=status.HTTP_200_OK)

        response['error'] = ['Not Authorized']
        return Response(response, status=status.HTTP_401_UNAUTHORIZED)


class UserLogout(generics.GenericAPIView):
    serializer_class = UserLogoutSerializer

    def post(self, request, *args, **kw):
        token = self.request.data.get(key_config.KEY_TOKEN)
        response = {}
        serializer_data = UserLogoutSerializer(data=self.request.data)
        if serializer_data.is_valid():
            AuthToken.objects.expire_token(token=token)
            return Response(response, status=status.HTTP_200_OK)
        else:
            response['error'] = serializer_data.errors
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)



class UserForgotPassword(generics.GenericAPIView):
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        username = self.request.data.get(key_config.KEY_USERNAME)
        user = User.objects.filter(
            username__iexact=username)
        response ={}
        if not user:
            response['error'] = ['User-name Not registered']
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        else:
            user = user.first()
            auth_token = AuthToken.objects.create(user=user)

            action_url = FRONTEND_URL + '/ChangePassword?token='+str(auth_token.token)

            msg_html = render_to_string('forgot_password_email.html', {'action_url': action_url,'product_name':PRODUCT_NAME})

            send_mail(
                'email title',
                msg_html,
                FROM_EMAIL,
                [user.username],
                html_message=msg_html,
            )
            response['success'] = 'Check the registered email for more details'
            return Response(response, status=status.HTTP_200_OK)





class UserResetPassword(generics.UpdateAPIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ResetPasswordSerializer
    model = User

    def get_object(self):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {}
            response['success'] = 'Password successfully changed'
            return Response(response, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)