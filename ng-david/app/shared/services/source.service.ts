import { Injectable } from '@angular/core';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppSettings} from '../../app.constant';


@Injectable()
export class SourceService {
  private apiUrl: string = AppSettings.API_ENDPOINT;
  
  constructor(private http: Http){
  }

  /**
   * Log the user in
   */
  getSources(): Observable<string> {
    let authToken = localStorage.getItem('api_auth_token');
    let headers = new Headers({ 'Accept': 'application/json' });
    headers.append('Authorization', `${authToken}`);

    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${this.apiUrl}/uploader/list`, options)
      .map(res => res.json())
      .do(res => {
          console.log('here')
          console.log(res)
      })
      .catch(this.handleError);
  }

  /**
   * Handle any errors from the API
   */
  private handleError(err) {
    let errMessage: string;

    if (err instanceof Response) {
      let body   = err.json() || '';
      let error  = body.error || JSON.stringify(body);
      errMessage = `${err.statusText || ''} ${error}`;
    } else {
      errMessage = err.message ? err.message : err.toString();
    }

    return Observable.throw(errMessage);
  }

}