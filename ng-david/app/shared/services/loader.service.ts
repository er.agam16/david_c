import { Injectable } from '@angular/core';
import {Http, Response, RequestOptions, Headers, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppSettings} from '../../app.constant';


@Injectable()
export class LoaderService {
  private apiUrl: string = AppSettings.API_ENDPOINT + '/loader';
  
  constructor(private http: Http){
  }

  /**
   * Log the user in
   */
  getLoaders(): Observable<string> {
    let authToken = localStorage.getItem('api_auth_token');
    let headers = new Headers({ 'Accept': 'application/json' });
    headers.append('Authorization', `${authToken}`);
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${this.apiUrl}/api/LoaderList/`, options)
      .map(res => res.json())
      .do(res => {
          console.log(res)
      })
      .catch(this.handleError);
  }

  /**
   * Log the user in
   */
  getLoaderValidator(files): Observable<string> {
    let authToken = localStorage.getItem('api_auth_token');
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', `${authToken}`);
    let options = new RequestOptions({ headers: headers });
    let urlSearchParams = new URLSearchParams();
    urlSearchParams.append('data',  files.join());
    urlSearchParams.append('is_header', '1');
    let body = urlSearchParams.toString()
   
    return this.http.post(`${this.apiUrl}/api/MultipleFileToRedShiftValidate/`, body, options)
      .map(res => res.json())
      .do(res => {
          console.log(res)
      })
      .catch(this.handleError);
  }

  /**
   * Handle any errors from the API
   */
  private handleError(err) {
    let errMessage: string;

    if (err instanceof Response) {
      let body   = err.json() || '';
      let error  = body.error || JSON.stringify(body);
      errMessage = `${error}`;
    } else {
      errMessage = err.message ? err.message : err.toString();
    }

    return Observable.throw(errMessage);
  }

}