import { Component, OnInit } from '@angular/core';
import { TargetService } from '../shared/services/target.service'
import { ToastrService } from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  templateUrl: './app/target/target-add.component.html'
})
export class UserTargetAddComponent implements OnInit{
	database_form  = {
		target_name: null,
		name: null,
		host: null,
		port: null,
		username: null,
		password: null
			
	}
	constructor(private targetService: TargetService, private toastrService: ToastrService, private router:Router){}
	ngOnInit() {}

	  /**
	   * Login a user
	   */
	login() {
		this.targetService.addDatabase(this.database_form)
		.subscribe(
			data => {
				this.toastrService.success('New Target Database is added', 'Success');
				this.router.navigate(['/targets']);
			},
			err => {
				this.toastrService.error(err, 'Error');
			}
		);
	}
}