import { Component, OnInit } from '@angular/core';
import { TargetService } from '../shared/services/target.service'
import { ToastrService } from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  templateUrl: './app/target/target-list.component.html'
})
export class UserTargetListComponent implements OnInit{
	constructor(private targetService: TargetService, private toastrService: ToastrService, private router:Router){}
	targetist;
	ngOnInit() {
	   this.refreshList()
	  }

	  refreshList() {
	  	this.targetService.getTargets()
	   .subscribe(
	       data => this.targetist = data,
	       err => {
	         console.log('Error ' + err)
	       }
	     )
	  }

	  deleteTarget(id) {
	  	this.targetService.deleteTarget(id)
		.subscribe(
			data => {
				this.refreshList()
				this.toastrService.success('Target Database is deleted', 'Success');
			},
			err => {
				this.toastrService.error(err, 'Error');
			}
		);
	  }

	  testConnection(id) {
	  	this.targetService.testConnection(id)
		.subscribe(
			data => {
				this.toastrService.success(data.msg, 'Success');
			},
			err => {
				this.toastrService.error(err, 'Error');
			}
		);
	  }

}