import { Component, OnInit } from '@angular/core';
import { TargetService } from '../shared/services/target.service'
import { ToastrService } from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  templateUrl: './app/target/target-edit.component.html'
})
export class UserTargetEditComponent implements OnInit{
	private dbId;
	database_form  = {
		id: null,
		target_name: null,
		name: null,
		host: null,
		port: null,
		username: null,
		password: null
			
	}
	constructor(private targetService: TargetService, private toastrService: ToastrService, private router:Router, private route:ActivatedRoute){}
	ngOnInit() {
		this.dbId = parseInt(this.route.snapshot.params['id']);
	   this.targetService.getTargets(this.dbId)
	   .subscribe(
	       data => {
	       	if(typeof data == 'undefined')
	       	{
	       		this.toastrService.error('Invalid Id Selected', 'Error');
			this.router.navigate(['/targets']);
	       	}
	       	this.database_form.target_name = data.target_name;
	       	this.database_form.id = data.id;
	       	this.database_form.name = data.name;
	       	this.database_form.host = data.host;
	       	this.database_form.port = data.password;
	       	this.database_form.username = data.username;
	       	this.database_form.password = data.password;
	       },
	       err => {
	        this.toastrService.error('Error Occurred. Please try again', 'Error');
			this.router.navigate(['/targets']);
	       }
	     )
	  }

	  /**
	   * Login a user
	   */
	login() {
		this.targetService.editDatabase(this.dbId, this.database_form)
		.subscribe(
			data => {
				this.toastrService.success('Target Database is updated', 'Success');
				this.router.navigate(['/targets']);
			},
			err => {
				this.toastrService.error(err, 'Error');
			}
		);
	}
}