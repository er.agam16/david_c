import  {NgModule} from '@angular/core';
import  {BrowserModule} from '@angular/platform-browser';
import  {AppComponent} from './app.component';
import {UserProfileComponent} from './users/user-profile.component';
import {UserFormComponent} from './users/user-form.component';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AboutComponent} from './about/about.component';
import {UserSourceComponent} from './source/source.component';
import {UserSourceAddComponent} from './source/source-add.component';
import {NotFoundComponent} from './not-found/notfound.component';
import {appRouting} from './app.routing';
import {HomeModule} from './home/home.module'
import {AuthGuard} from './shared/guards/auth-guard.service'
import {CanDeactivateGuard} from './shared/guards/can-deactivate-guard.service'
import { FileUploadModule } from 'ng2-file-upload';

// BrowserAnimationsModule is required
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
import { LaddaModule } from 'angular2-ladda';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';

@NgModule({
	imports : [
		BrowserModule, FormsModule, appRouting, HomeModule, 
		HttpModule, BrowserAnimationsModule,
    		ToastrModule.forRoot(), FileUploadModule, LaddaModule
	],
	declarations : [
		AppComponent, UserProfileComponent, UserFormComponent, 
		AboutComponent, UserSourceComponent, NotFoundComponent, UserSourceAddComponent
	],
	providers: [AuthGuard, CanDeactivateGuard],
	bootstrap : [AppComponent]
})

export class AppModule {}