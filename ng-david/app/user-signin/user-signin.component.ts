import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../shared/services/user.service';
import {User} from '../shared/models/user';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: './app/user-signin/user-signin.component.html'
})
export class UserSignInComponent implements OnInit{
	credentials = { username: '', password: '' };
	constructor(
		private userService: UserService,
		private router:Router,
		private toastrService: ToastrService
	){}
	ngOnInit(){}

  /**
   * Login a user
   */
	login() {
		this.userService.login(this.credentials.username, this.credentials.password)
		.subscribe(
			data => {
				this.toastrService.success('Logged In', 'Success');
				this.router.navigate(['/sources']);
			},
			err => {
				this.toastrService.error(err, 'Error');
			}
		);
	}
}