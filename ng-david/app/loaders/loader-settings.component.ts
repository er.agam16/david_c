import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../shared/services/loader.service'
import {ActivatedRoute, Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: './app/loaders/loader-settings.component.html',
  styles: [`.spinner {
  margin: 10px auto 0;
  width: 70px;
  text-align: center;
}

.spinner > div {
  width: 18px;
  height: 18px;
  background-color: #fff;

  border-radius: 100%;
  display: inline-block;
  -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay 1.4s infinite ease-in-out both;
}

.spinner .bounce1 {
  -webkit-animation-delay: -0.32s;
  animation-delay: -0.32s;
}

.spinner .bounce2 {
  -webkit-animation-delay: -0.16s;
  animation-delay: -0.16s;
}

@-webkit-keyframes sk-bouncedelay {
  0%, 80%, 100% { -webkit-transform: scale(0) }
  40% { -webkit-transform: scale(1.0) }
}

@keyframes sk-bouncedelay {
  0%, 80%, 100% { 
    -webkit-transform: scale(0);
    transform: scale(0);
  } 40% { 
    -webkit-transform: scale(1.0);
    transform: scale(1.0);
  }
}`]
})
export class LoaderSettingComponent implements OnInit{
	constructor(private loaderService: LoaderService, private router:Router, private toastrService: ToastrService){
                localStorage.setItem('loaderSettingsForm', JSON.stringify({}));
            }
	loaderData;
            loader_sett_form  = {
                loader_name: null,
                delimeter: ',',
                is_header: null
            }
	ngOnInit() {
		let files = JSON.parse(localStorage.getItem('selectedFiles'));
		if(files.length == 0)
                        {
                                this.toastrService.error('Files not selected. Please select some source files', 'Error');
                                this.router.navigate(['/loaders']);
                        } else
                        {
                                this.loaderService.getLoaderValidator(files)
                               .subscribe(
                                   data => {
                                       this.loaderData = data
                                   },
                                   err => {
                                       this.toastrService.error('Headers are not equal. Please select some other files', 'Error');
                                       this.router.navigate(['/loaders']);
                                     console.log('Error ' + err)
                                   }
                                 )
                        }
	  }

      loadToRedshift() {
          localStorage.setItem('loaderSettingsForm', JSON.stringify(this.loader_sett_form));
          this.router.navigate(['/load-select-db']);
      }

}