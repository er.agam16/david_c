import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../shared/services/loader.service'

@Component({
  templateUrl: './app/loaders/loader-list.component.html'
})
export class LoaderListComponent implements OnInit{
	constructor(private loaderService: LoaderService){}
	loaderList;
	ngOnInit() {
		this.loaderService.getLoaders()
   		.subscribe(
   				data => this.loaderList = data,
       			err => {
         				console.log('Error ' + err)
       			}
		)
		setInterval(() => {
	   		this.loaderService.getLoaders()
	   		.subscribe(
       				data => this.loaderList = data,
	       			err => {
	         				console.log('Error ' + err)
	       			}
			)
	   	}, 10000); 	
  	}

}