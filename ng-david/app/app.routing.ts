import {ModuleWithProviders} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UserSourceComponent } from './source/source.component';
import { UserSourceAddComponent } from './source/source-add.component';
import { UserTargetListComponent } from './target/target.component';
import { UserTargetAddComponent } from './target/target-add.component';
import { UserTargetEditComponent } from './target/target-edit.component';

import { LoaderListComponent } from './loaders/loader.component';
import { LoaderSettingComponent } from './loaders/loader-settings.component';

import {NotFoundComponent} from './not-found/notfound.component';
import {UserSignUpComponent} from './user-signup/user-signup.component';
import {UserSignInComponent} from './user-signin/user-signin.component';
import { AuthGuard } from './shared/guards/auth-guard.service';
import {UserSourceLoadRedComponent} from './source/source-red.component';



const appRoutes: Routes = [
  { path: '', component: HomeComponent, data : {cloudBackgroundShow : true, indexContentShow : true} },
  { path: 'signup', component: UserSignUpComponent },
  { path: 'signin', component: UserSignInComponent },
  { path: 'sources', component: UserSourceComponent, canActivate: [AuthGuard], data : {cloudBackgroundShow : false} },
  { path: 'sources/add', component: UserSourceAddComponent, canActivate: [AuthGuard], data : {cloudBackgroundShow : false} },
  { path: 'load-select-db', component: UserSourceLoadRedComponent, canActivate: [AuthGuard], data : {cloudBackgroundShow : false} },
  { path: 'targets', component: UserTargetListComponent, canActivate: [AuthGuard], data : {cloudBackgroundShow : false} },
  { path: 'loaders', component: LoaderListComponent, canActivate: [AuthGuard], data : {cloudBackgroundShow : false} },
  { path: 'targets/add', component: UserTargetAddComponent, canActivate: [AuthGuard], data : {cloudBackgroundShow : false} },
  { path: 'targets/edit/:id', component: UserTargetEditComponent, canActivate: [AuthGuard], data : {cloudBackgroundShow : false} },
  { path: 'loader/settings', component: LoaderSettingComponent, canActivate: [AuthGuard], data : {cloudBackgroundShow : false} },
  { path: '**', component: NotFoundComponent }
];

export const appRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);