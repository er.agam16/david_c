import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {HomeComponent} from './home.component';
import {UserService} from '../shared/services/user.service';
import {SourceService} from '../shared/services/source.service';
import {TargetService} from '../shared/services/target.service';
import {LoaderService} from '../shared/services/loader.service';
import {UserSignUpComponent} from '../user-signup/user-signup.component';
import {UserSignInComponent} from '../user-signin/user-signin.component';
import {homeRouting} from './home.routing';
import {MainHomeComponent} from '../main-home/main-home.component'
import {FormsModule} from '@angular/forms';
import { UserTargetListComponent } from '../target/target.component';
import { UserTargetAddComponent } from '../target/target-add.component';
import { UserTargetEditComponent } from '../target/target-edit.component';
import {LoaderListComponent} from '../loaders/loader.component';
import {LoaderSettingComponent} from '../loaders/loader-settings.component';
import {UserSourceLoadRedComponent} from '../source/source-red.component'
import { LaddaModule } from 'angular2-ladda';

@NgModule({
	imports : [homeRouting, CommonModule, FormsModule, LaddaModule],
	declarations : [
		HomeComponent,
		UserSignUpComponent,
		UserSignInComponent,
		MainHomeComponent,
		UserTargetListComponent,
		UserTargetAddComponent,
		UserSourceLoadRedComponent,
		LoaderListComponent,
		UserTargetEditComponent,
		LoaderSettingComponent
	],
	providers: [UserService, SourceService, TargetService, LoaderService],
})

export class HomeModule{}