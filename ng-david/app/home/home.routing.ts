import {ModuleWithProviders} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
// import {UserInfoComponent} from '../user-info/user-info.component';
import {MainHomeComponent} from '../main-home/main-home.component'
const homeRoutes: Routes = [];

export const homeRouting: ModuleWithProviders = RouterModule.forRoot(homeRoutes);