import { Component, OnInit } from '@angular/core';
import { TargetService } from '../shared/services/target.service'
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  templateUrl: './app/source/source-select-db.component.html'
})
export class UserSourceLoadRedComponent implements OnInit{
	loader_form = {
		target_id : '',
		table_name : '',
	}
	constructor(
		private toastrService: ToastrService, 
		private targetService: TargetService, 
		private route: ActivatedRoute, 
		private router: Router
	){}
	targetList;
	ngOnInit() {
	   this.targetService.getTargets()
	   .subscribe(
	       data => {
       		this.targetList = data
	       },
	       err => {
	         console.log('Error ' + err)
	       }
	     )
	  }
	  // trigger-variable for Ladda
    isLoading: boolean = false;
    loaderSetting = localStorage.getItem('loaderSettingsForm');
    selectedFiles = localStorage.getItem('selectedFiles');
  loadToRedshift() {
  	this.isLoading = true;
  	this.targetService.loadToRedshift(this.selectedFiles, this.loader_form.target_id, this.loader_form.table_name, this.loaderSetting)
	.subscribe(
		data => {
			this.isLoading = false;
			this.toastrService.success('Uploaded To Redshift', 'Success');
			this.router.navigate(['/loaders']);
		},
		err => {
			this.isLoading = false;
			this.toastrService.error(err, 'Error');
		}
	);
  }

}