import { Component, OnInit } from '@angular/core';
import { SourceService } from '../shared/services/source.service'
import { FileUploader } from 'ng2-file-upload';
import {AppSettings} from '../app.constant';

const URL = AppSettings.API_ENDPOINT + '/uploader/upload/';

@Component({
	templateUrl: './app/source/source-add.component.html'
})
export class UserSourceAddComponent implements OnInit{
	authToken: string;
	public uploader:FileUploader;
	constructor(private sourceService: SourceService){
		this.authToken = localStorage.getItem('api_auth_token');
		this.uploader = new FileUploader({
			url: URL + 'uploading', 
			filters: [{
				name: 'extension',
				fn: (item: any): boolean => {
					const fileExtension = item.name.slice(item.name.lastIndexOf('.') + 1).toLowerCase();
					return fileExtension === 'csv' ;
				}
			}],
			method: 'POST',
			autoUpload: true,
			headers: [{
				name: 'Authorization',
				value : this.authToken
			}],
		});
		this.uploader.onBuildItemForm = (item, form) => {
			this.uploader.queue.forEach((elem)=> {
				// console.log(elem.file.name)
			           elem.url = URL + elem.file.name;
		        	});
		};
	}
	public hasBaseDropZoneOver:boolean = false;
	public hasAnotherDropZoneOver:boolean = false;

	public fileOverBase(e:any):void {
		this.hasBaseDropZoneOver = e;
	}
	ngOnInit() {
	}

}