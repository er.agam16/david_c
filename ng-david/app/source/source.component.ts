import { Component, OnInit } from '@angular/core';
import { SourceService } from '../shared/services/source.service'
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  templateUrl: './app/source/source.component.html',
  styles: [`.spinner {
  margin: 10px auto 0;
  width: 70px;
  text-align: center;
}

.spinner > div {
  width: 18px;
  height: 18px;
  background-color: #fff;

  border-radius: 100%;
  display: inline-block;
  -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay 1.4s infinite ease-in-out both;
}

.spinner .bounce1 {
  -webkit-animation-delay: -0.32s;
  animation-delay: -0.32s;
}

.spinner .bounce2 {
  -webkit-animation-delay: -0.16s;
  animation-delay: -0.16s;
}

@-webkit-keyframes sk-bouncedelay {
  0%, 80%, 100% { -webkit-transform: scale(0) }
  40% { -webkit-transform: scale(1.0) }
}

@keyframes sk-bouncedelay {
  0%, 80%, 100% { 
    -webkit-transform: scale(0);
    transform: scale(0);
  } 40% { 
    -webkit-transform: scale(1.0);
    transform: scale(1.0);
  }
}`]
})
export class UserSourceComponent implements OnInit{
	constructor(private sourceService: SourceService, private router:Router){
        localStorage.setItem('selectedFiles', JSON.stringify([]));
      }
	fileList;
      showAddLoaderButton: boolean = false;
	loadingData: boolean = true
	ngOnInit() {
	   this.sourceService.getSources()
	   .subscribe(
	       data => {this.fileList = data.files;this.loadingData=false},
	       err => {
	         console.log('Error ' + err)
	       }
	     )
	  }

    getSelectedChecks() {
      let that = this;
      that.showAddLoaderButton = false;
      that.fileList.forEach(function(fileData) {
        if(typeof fileData.checked != 'undefined' && fileData.checked == true)
        {
          that.showAddLoaderButton = true;
        }
      })
    }

        loadToRedshift() {
          let that = this;
          let selectedArray = [];
          that.fileList.forEach(function(fileData) {
            if(typeof fileData.checked != 'undefined' && fileData.checked == true)
            {
              selectedArray.push(fileData.filename);
            }
          })
          localStorage.setItem('selectedFiles', JSON.stringify(selectedArray));
          this.router.navigate(['/loader/settings']);
        }

}