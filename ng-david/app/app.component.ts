import {Component, OnInit} from '@angular/core';
import { UserService } from './shared/services/user.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/pairwise';

@Component({
  selector: 'my-app',
  templateUrl: './app/app.component.html',
  styleUrls: []
})
export class AppComponent implements OnInit{
	cloudBackgroundShow: boolean = false;
	indexContentShow: boolean =  false;
      subscribeEmail = '';
	constructor(
	    private userService: UserService, 
	    private router: Router,
	    private route: ActivatedRoute,
          private toastrService: ToastrService
	) {
		this.router.events
		    .filter(event => event instanceof NavigationEnd)
		    .subscribe((event:NavigationEnd) => {
		      // You only receive NavigationStart events
		      console.log()
		    });

	}


	ngOnInit() {
        console.log(this.subscribeEmail)
	    this.router.events
	      .filter((event) => event instanceof NavigationEnd)
	      .map(() => this.route)
	      .map((route) => {
	        while (route.firstChild) route = route.firstChild;
	        return route;
	      })
	      .filter((route) => route.outlet === 'primary')
	      .mergeMap((route) => route.data)
	      .subscribe((event) => {
	      	this.indexContentShow = typeof(event.indexContentShow) == 'undefined' ? 0 : event.indexContentShow;
		this.cloudBackgroundShow = typeof(event.cloudBackgroundShow) == 'undefined' ? 1 : event.cloudBackgroundShow;
      	       });
	  }


/**
   * Is the user logged in?
   */
  get indexContentShowF() {
    return this.indexContentShow;
  }

  /**
   * Is the user logged in?
   */
  get cloudBackgroundShowF() {
    return this.cloudBackgroundShow;
  }



	/**
   * Is the user logged in?
   */
  get isLoggedIn() {
    return this.userService.isLoggedIn();
  }

  	/**
   * Is the user logged in?
   */
  get loggedInData() {
    return this.userService.getLoggedInData();
  }

  /**
   * Log the user out
   */
  logout() {
    this.userService.logout();
    this.router.navigate(['/signin']);
  }

    /**
   * Log the user out
   */
  onSubmit() {
    this.userService.subscribeEmail(this.subscribeEmail).subscribe(
      data => {
        this.subscribeEmail = '';
        this.toastrService.success('Registered', 'Success');
      },
      err => {
        this.subscribeEmail = '';
        this.toastrService.success('Registered', 'Success');
      }
    );
  }
}