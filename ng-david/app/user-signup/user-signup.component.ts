import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../shared/services/user.service';
import {User} from '../shared/models/user';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: './app/user-signup/user-signup.component.html',
  styleUrls: ['./app/app.component.css']
})
export class UserSignUpComponent{
	user: User = new User();
	constructor(
		private route: ActivatedRoute, 
		private userService: UserService,
		private router:Router,
		private toastrService: ToastrService
	){}
	register() {
		this.userService.register(this.user.username, this.user.password, this.user.last_name, this.user.first_name)
		.subscribe(
			data => {
				this.toastrService.success('Registered', 'Success');
				this.router.navigate(['/sources']);
			},
			err => {
				this.toastrService.error(err, 'Error');
			}
		);
	}
}